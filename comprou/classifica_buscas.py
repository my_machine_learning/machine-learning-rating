import pandas as pd

 # x% dos dados
PORCENTAGEM_TREINO = 0.9 

data_frame = pd.read_csv('buscas2.csv')

X_data_frame = data_frame[['home','busca','logado']]

# convertemos os valores do data frame para inteiro
# cria as categorias para coluna busca -> gera dummies
x_data_frame_dummies = pd.get_dummies(X_data_frame).astype(int)

# a coluna comprou nao precisa de passar por categorizacao
y_data_frame_dummies = data_frame['comprou']

# convertemos de data frame para array
X = x_data_frame_dummies.values
Y = y_data_frame_dummies.values

# dados de treino - 90% do dados
tamanho_treino = int(PORCENTAGEM_TREINO * len(X))
treino_dados = X[:tamanho_treino]

# dados de marcacoes
treino_marcacoes = Y[:tamanho_treino]


# dados de teste - resto dos dados
tamanho_teste = len(X) - tamanho_treino
teste_dados = X[-tamanho_teste:]
teste_marcacoes = Y[-tamanho_teste:]

# criamos o modelo
from sklearn.naive_bayes import MultinomialNB
modelo = MultinomialNB()
modelo.fit(treino_dados, treino_marcacoes)

# pedimos o resultado
resultado = modelo.predict(teste_dados)

# avaliamos o resultado : retorna um array de boleanos
# para cada posicao comparada, retorna true se forem igual (sim=sim)
# ou retorna false se foram diferentes 
diferencas = resultado == teste_marcacoes

acertos = [d for d in diferencas if d == True ]
total_de_acertos = len(acertos)
total_de_elementos = len(teste_dados)

taxa_de_acerto = 100.0 * total_de_acertos / total_de_elementos

print(taxa_de_acerto)