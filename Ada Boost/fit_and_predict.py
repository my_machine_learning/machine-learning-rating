import numpy as np

def fit_and_predict(modelo, treino_dados, treino_marcacoes, teste_dados, teste_marcacoes):
    
    modelo.fit(treino_dados, treino_marcacoes)

    resultado = modelo.predict(teste_dados)
    
    # torna uma lista de array em um array
    teste_marcacoes = np.concatenate(teste_marcacoes)

    # avaliamos o resultado : retorna um array de boleanos
    # para cada posicao comparada, retorna true se forem igual (sim=sim)
    # ou retorna false se foram diferentes 
    acertos = resultado == teste_marcacoes


    total_de_acertos = sum(acertos)
    total_de_elementos = len(teste_dados)
    taxa_acertos = (100.0 * total_de_acertos / total_de_elementos)

    return taxa_acertos

