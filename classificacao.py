#!/usr/bin/env python
#coding: utf-8

from data import build
x,y = build()

# separando dados para testes e dados para treino
# 90% para treinamento 10% para testes

training_data = x[:90]
training_marking = y[:90]

test_data = x[-9:]
test_marking = y[-9:]

from sklearn.naive_bayes import MultinomialNB
model = MultinomialNB()
model.fit(training_data,training_marking)

# usando para treinar e testar o mesmo conjunto 
# esperamos um rate hit auto portanto

result_predict = model.predict(test_data)

#faco a diferença entre o que foi previsto com o que aconteceu de fato
diff = result_predict - test_marking

# se a diferenca eh 0 -> o modelo acertou
hits = [a for a in diff if a == 0]
total_hits = len(hits)
total_test_elements = len(test_data)

rate_hit = (100.0 * total_hits) / total_test_elements

print(rate_hit)
# rate = 88.89% de acerto